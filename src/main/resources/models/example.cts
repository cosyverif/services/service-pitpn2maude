parameters a >= 0 and a <= 4
initially { int itemready=0, buffer=0, readyproducer=1, itemreceived=0, readyconsumer=1; }
transition [ intermediate { readyproducer = readyproducer - 1; }, speed=1*(1) ] produce [2,6]
      when (readyproducer >= 1)
      { readyproducer = readyproducer - 1, itemready = itemready + 1; }
transition [ intermediate { itemready = itemready - 1; }, speed=1*(1) ] send [2,4]
      when (itemready >= 1)
      { itemready = itemready - 1, readyproducer = readyproducer + 1, buffer = buffer + 1; }
transition [ intermediate { buffer = buffer - 1, readyconsumer = readyconsumer - 1; }, speed=1*(1) ] receive [a,a]
      when (buffer >= 1 and readyconsumer >= 1)
      { buffer = buffer - 1, readyconsumer = readyconsumer - 1, itemreceived = itemreceived + 1; }
transition [ intermediate { itemreceived = itemreceived - 1; }, speed=1*(1) ] consume [0,0]
      when (itemreceived >= 1)
      { itemreceived = itemreceived - 1, readyconsumer = readyconsumer + 1; }
// insert TCTL formula here : check formula