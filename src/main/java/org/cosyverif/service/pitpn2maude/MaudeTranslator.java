package org.cosyverif.service.pitpn2maude;

import java.io.File;
import java.nio.file.Paths;

import org.apache.commons.exec.CommandLine;
import org.cosyverif.alligator.service.AnnotatedService.Task;
import org.cosyverif.alligator.service.Parameter.Direction;
import org.cosyverif.alligator.service.annotation.Example;
import org.cosyverif.alligator.service.annotation.Launch;
import org.cosyverif.alligator.service.annotation.Parameter;
import org.cosyverif.alligator.service.annotation.Service;
import org.cosyverif.alligator.service.output.ParameterOutputHandler;
import org.w3c.dom.DOMException;

@Service(name = "Generate Maude encoding", 
    help = "Generate Maude encoding of the Petri net", 
    version = "1.0", 
    tool = "PITPN2Maude", 
    index = 2,
    authors = {
        "Jaime Arias",
        "Kyungmin Bae",
        "Carlos Olarte",
        "Peter Ölveczky",
        "Laure Petrucci",
        "Fredrik Rømming"
    }, 
    keywords = {})
public final class MaudeTranslator extends Maude {
  // output
  @Parameter(name = "Maude file", help = "File with the output", direction = Direction.OUT, contenttype = "plain/text")
  public File MaudeFile = null;

  @Launch
  public Task executeBinary() throws DOMException, Exception {
    // parse GrML model
    parser = GrmlParser.create(model);
    final File model = parser.parseToXML();

    final CommandLine command = new CommandLine("python3");
    command.addArguments("app.py");
    command.addArguments("--input " + model.getAbsoluteFile());
    command.addArguments("--theory " + parseTheory(theory));

    final File wdir = Paths.get(baseDirectory().getAbsolutePath().toString(), "pitpn2maude").toFile();
    final File outputFilePath = Paths.get(wdir.toString(), "model.maude").toFile();
    return task(command)
        .workingDirectory(wdir)
        .out(new ParameterOutputHandler(this) {
          @Override
          public void fallback(String key, String value) {
            if (outputFilePath.exists()) {
              MaudeFile = outputFilePath;
            }
          }
        });
  }

  @Example(name = "Example of the Maude translation", help = "Example of the Maude translation")
  public MaudeTranslator example() {
    MaudeTranslator service = new MaudeTranslator();
    service.model = loadModelResource("/models/example2.grml");
    service.theory = Theory.R1_Folding;
    return service;
  }

}
