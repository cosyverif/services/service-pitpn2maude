package org.cosyverif.service.pitpn2maude;

import java.io.File;

import org.apache.commons.exec.CommandLine;
import org.cosyverif.alligator.service.AnnotatedService.Task;
import org.cosyverif.alligator.service.Parameter.Direction;
import org.cosyverif.alligator.service.annotation.Example;
import org.cosyverif.alligator.service.annotation.Launch;
import org.cosyverif.alligator.service.annotation.Parameter;
import org.cosyverif.alligator.service.annotation.Service;
import org.cosyverif.alligator.service.output.LineOutputHandler;
import org.w3c.dom.DOMException;

@Service(name = "CheckPTCTL", 
    help = "Maude PTCTL verification", 
    version = "1.0", 
    tool = "PITPN2Maude", 
    index = 1,
    authors = {
      "Jaime Arias",
      "Kyungmin Bae",
      "Carlos Olarte",
      "Peter Ölveczky",
      "Laure Petrucci",
      "Fredrik Rømming"
    }, 
    keywords = {})
public final class CheckPTCTL extends Maude {
  // SMT solver
  @Parameter(name = "SMT Solver", help = "SMT solver", direction = Direction.IN, underlying = Solver.class)
  public Solver solver = Solver.Yices2;

  // Timeout
  @Parameter(name = "Timeout", help = "Timeout in ms", direction = Direction.IN)
  public int timeout = 10;

  // output
  @Parameter(name = "Result", help = "Result from checking formula", direction = Direction.OUT, multiline = true)
  public String result = "";

  @Launch
  public Task executeBinary() throws DOMException, Exception {
    // parse GrML model
    parser = GrmlParser.create(model);
    final File model = parser.parseToXML();

    final CommandLine command = new CommandLine("./cli.sh");
    command.addArguments("-r");
    command.addArgument(model.getAbsolutePath()); // romeo model
    command.addArgument(parseTheory(theory)); // maude theory
    command.addArgument(parseSolver(solver)); // SMT solver
    command.addArgument(timeout + "m"); // timeout in ms

    // run command
    return task(command)
        .workingDirectory(baseDirectory())
        .out(new LineOutputHandler(this) {
          @Override
          public void call(String line) {
            if (!line.startsWith("Output file:")) {
              result += line + "\n";
            }
          }
        });
  }

  @Example(name = "Example of Maude PTCTL verification", help = "Example of Maude PTCTL verification")
  public CheckPTCTL example() {
    CheckPTCTL service = new CheckPTCTL();
    service.model = loadModelResource("/models/example2.grml");
    service.theory = Theory.R0;
    service.solver = Solver.Yices2;
    service.timeout = 1;
    return service;
  }
}
