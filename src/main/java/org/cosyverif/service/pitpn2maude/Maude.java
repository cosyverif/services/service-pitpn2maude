package org.cosyverif.service.pitpn2maude;

import org.cosyverif.alligator.service.BinaryService;
import org.cosyverif.alligator.service.Parameter.Direction;
import org.cosyverif.alligator.service.annotation.Parameter;
import org.cosyverif.model.Model;

public abstract class Maude extends BinaryService {
  GrmlParser parser;

  // Maude theories
  enum Theory {
    R0,
    R1,
    R1_Folding
  }

  // SMT solvers
  enum Solver {
    Yices2,
    Z3
  }

  // Model file
  @Parameter(name = "Model", help = "Petri net model", direction = Direction.IN, formalism = "http://formalisms.cosyverif.org/parametric-pt-net.fml")
  public Model model;

  // Maude theory
  @Parameter(name = "Maude Theory", help = "Maude Theory", direction = Direction.IN, underlying = Theory.class)
  public Theory theory = Theory.R0;

  /**
   * Return the string representation of a theory enum
   *
   * @param t Theory enum
   * @return String
   * @throws Exception
   */
  String parseTheory(Theory t) throws Exception {
    switch (t) {
      case R0:
        return "symbolic-theory";
      case R1:
        return "symbolic-theory2";
      case R1_Folding:
        return "symbolic-folding-tree";
      default:
        throw new Exception(t + " is not handled by the tool");
    }
  }

  /**
   * Return the string representation of the Solver enum
   *
   * @param s Solver enum
   * @return String
   * @throws Exception
   */
  String parseSolver(Solver s) throws Exception {
    switch (s) {
      case Yices2:
        return "yices2";
      case Z3:
        return "z3";
      default:
        throw new Exception(s + " is not handled by the tool");
    }
  }
}
