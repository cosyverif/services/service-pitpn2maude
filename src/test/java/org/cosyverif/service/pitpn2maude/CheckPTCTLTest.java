package org.cosyverif.service.pitpn2maude;

import java.io.File;

import org.cosyverif.Configuration;
import org.cosyverif.alligator.service.AnnotatedService;
import org.cosyverif.alligator.util.Utility;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class CheckPTCTLTest {
  private File directory;

  /** Create temp folder */
  @Before
  public void setUp() {
    directory = Utility.createTemporaryDirectory();
    Configuration.instance()
        .temporaryDirectory(directory);
  }

  /** Delete temp folder */
  @After
  public void tearDown() {
    Utility.deleteDirectory(directory);
    Configuration.instance()
        .temporaryDirectory(new File(System.getProperty("java.io.tmpdir")));
  }

  /** Test that the service can be instantiated */
  @Test
  public void testConstructor() {
    try {
      new CheckPTCTL();
    } catch (Exception e) {
      e.printStackTrace();
      Assert.fail("Unable to instantiate the CheckPTCTL service.");
    }
  }

  /**
   * Test the service with the example
   */
  @Test
  public void testExample() {
    CheckPTCTL generator = new CheckPTCTL().example();
    AnnotatedService service = AnnotatedService.of(generator);
    service.configure(service.description());
    service.run();

    // check if output was created
    System.out.println(generator.result);
    Assert.assertTrue(generator.result.contains("Bye."));
    System.out.println("[SUCCESS] testExample");
  }
}
